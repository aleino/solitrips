import { action, createStore, thunk, computed, actionOn } from 'easy-peasy';

import API from '../api/api';
import RouteEngine from '../routeEngine';

let routeEngine;

const routeModel = {
  lines: {},
  stations: [],
  roads: [],
  solvedRoute: null,
  selectedDepartureStationId: null,
  selectedArrivalStationId: null,
  selectDepartureStation: action((state, id) => {
    state.selectedDepartureStationId = id;
    state.solvedRoute = routeEngine.solveRoute(
      id,
      state.selectedArrivalStationId
    );
  }),
  // onDepartureStationIdChange: actionOn((actions, storeActions))
  selectArrivalStation: action((state, id) => {
    state.selectedArrivalStationId = id;
    state.solvedRoute = routeEngine.solveRoute(
      state.selectedDepartureStationId,
      id
    );
  }),

  updateBaseData: action((state, payload) => {
    return { ...state.routing, ...payload };
  }),
  fetchBaseData: thunk(async (actions, payload) => {
    const basedata = await API.fetchBaseData();
    actions.updateBaseData(basedata);
    const { lines, roads, stations } = basedata;
    routeEngine = new RouteEngine(lines, roads, stations);
  }),
};

const storeModel = {
  routing: routeModel,
};

const store = createStore(storeModel);

export default store;
