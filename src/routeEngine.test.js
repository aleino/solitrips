const fs = require('fs');
import RouteEngine from './routeEngine';
import API from './api/api';

const testDataPath = 'public/basedata.json';
const testData = JSON.parse(fs.readFileSync(testDataPath));
const { lines, roads, stations } = testData;

describe('Test Data', () => {
  it('has lines, roads and stations', () => {
    expect(testData.lines);
    expect(testData.roads);
    expect(testData.stns);
  });
});

describe('Route Engine', () => {
  const routeEngine = new RouteEngine(lines, roads, stations);
  it('has lines, roads and stations', () => {
    expect(routeEngine.lines);
    expect(routeEngine.roads);
    expect(routeEngine.stations);
  });
  it('has edge durations are numbers between 1 and 10', () => {
    Object.values(routeEngine.edgeDurations).forEach((val) => {
      expect(val).toBeGreaterThanOrEqual(1);
      expect(val).toBeLessThanOrEqual(10);
    });
  });
  it('should calculate route', () => {
    expect(routeEngine.solveRoute('A', 'K').stations).toEqual([
      'A',
      'C',
      'E',
      'M',
      'L',
      'K',
    ]);
  });
});
