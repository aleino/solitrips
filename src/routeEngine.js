// const _ = require('lodash');
const priorityQueue = require('js-priority-queue');

class RouteEngine {
  constructor(lines, roads, stations) {
    this.lines = lines;
    this.roads = roads;
    this.stations = stations;

    this.edgeDurations = this._parseEdgeDurations();
    this.busGraph = this._createBusGraph();
  }

  _parseEdgeDurations = () => {
    const edgeDurations = {};
    this.roads.forEach((road) => {
      const id = [road.to, road.from].sort().join('');
      edgeDurations[id] = road.duration;
    });
    return edgeDurations;
  };

  _getEdgeDuration = (from, to) => {
    const id = [from, to].sort().join('');
    return this.edgeDurations[id];
  };

  _createBusGraph = () => {
    const graph = {};

    const parseEdges = (line, stations) => {
      for (let i = 0; i < stations.length - 1; i++) {
        const fromNode = stations[i];
        const toNode = stations[i + 1];
        const duration = this._getEdgeDuration(fromNode, toNode);
        const edge = [toNode, duration, line];
        if (graph[fromNode]) {
          graph[fromNode].push(edge);
        } else {
          graph[fromNode] = [edge];
        }
      }
    };

    Object.entries(this.lines).forEach(([line, stations]) => {
      parseEdges(line, stations);
      parseEdges(line, stations.reverse());
    });
    return graph;
  };

  solveRoute = (start, end) => {
    console.log(start, end);
    if (!start || !end) {
      return null;
    }

    const LINE_CHANGE_PENALTY = 0.1;
    const visited = new Set();
    visited.add(start);

    // Queue Schema: [totalCost, fromNode, route, line]
    const Queue = new priorityQueue({ comparator: (a, b) => a[0] - b[0] });
    Queue.queue([0, start, [start], []]);

    while (Queue) {
      const [totalCost, fromNode, route, lines] = Queue.dequeue();

      console.debug({
        totalCost,
        route,
        fromNode,
        lines,
      });

      if (fromNode == end) {
        return {
          totalCost,
          stations: route,
          lines,
        };
      }

      const edges = Object.values(this.busGraph[fromNode]);

      for (let edge of edges) {
        const [toNode, cost, line] = edge;

        if (visited.has(`${toNode}-${line}`)) {
          continue;
        }
        visited.add(`${toNode}-${line}`);

        const newTotalCost =
          lines[lines.length - 1] == line
            ? totalCost + cost
            : totalCost + cost + LINE_CHANGE_PENALTY;

        const newQueueTuple = [
          newTotalCost,
          toNode,
          [...route, toNode],
          [...lines, line],
        ];

        Queue.queue(newQueueTuple);
      }
    }

    return null;
  };
}

export default RouteEngine;
