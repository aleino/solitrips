import orderBy from 'lodash/orderBy';

const parseBaseData = (data) => {
  data['stations'] = orderBy(data.stations, 'name', 'asc');
  return data;
};

/**
 * Fetch lines, roads and stops
 */
const fetchBaseData = () => {
  const url = 'basedata.json';
  return fetch(url)
    .then((res) => res.json())
    .then((data) => parseBaseData(data))
    .catch((err) => {
      console.log(err);
    });
};

/**
 * Fetch Open Route data
 * @param {String} id Edge Id
 */
const fetchEdge = (id) => {
  const url = `/route-segments/${id}.json`;
  console.log('fetching', id);
  return fetch(url)
    .then((res) => res.json())
    .catch((err) => {
      console.log(err);
    });
};

const API = {
  fetchBaseData,
  fetchEdge,
};

export default API;
