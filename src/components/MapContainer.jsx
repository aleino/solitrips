import React, { Fragment, useEffect, useState } from 'react';
import Map from './Map';

import { useStoreState } from 'easy-peasy';

const MapContainer = () => {
  const { stations, solvedRoute } = useStoreState((store) => store.routing);
  const [highlighted, setHighlighted] = useState(null);

  let stationsCollection = {
    type: 'FeatureCollection',
    features: [],
  };

  if (stations) {
    stations.forEach((s) => {
      stationsCollection.features.push({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: s.coordinates,
        },
        properties: {
          name: s.name,
        },
      });
    });
  }

  // TODO: Do not use harcoded edges
  const edges = [
    'AB',
    'BD',
    'AD',
    'AC',
    'CD',
    'CE',
    'DE',
    'EF',
    'FG',
    'GH',
    'HI',
    'IJ',
    'GI',
    'GK',
    'KL',
    'LM',
    'EM',
    'MN',
    'NO',
    'OP',
    'OQ',
    'PQ',
    'NQ',
    'QR',
    'NR',
    'DR',
  ];

  const colors = {
    keltainen: 'yellow',
    vihreä: 'lightgreen',
    punainen: 'red',
    sininen: 'blue',
  };

  const getHighlightedSegment = () => {
    const obj = {};
    const { stations, lines } = solvedRoute;

    for (let i = 0; i < stations.length - 1; i++) {
      const segments = stations[i] + stations[i + 1];
      const sortedSegment = segments
        .split('')
        .sort((a, b) => a.localeCompare(b))
        .join('');
      const color = colors[lines[i]];
      obj[sortedSegment] = color;
    }
    console.log(obj);
    return obj;
  };

  useEffect(() => {
    if (solvedRoute) {
      setHighlighted(getHighlightedSegment());
    }
  }, [solvedRoute]);

  return (
    <Fragment>
      <Map
        stations={stationsCollection}
        edges={edges}
        highlighs={highlighted}
      />
    </Fragment>
  );
};

export default MapContainer;
