import React, { useEffect, useRef, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import mapboxgl from 'mapbox-gl';

import stationMarker from '../station-marker.png';
import { colors } from '@material-ui/core';

const useStyles = makeStyles({
  map: {
    height: '100%',
    width: '100%',
    backgroundColor: '#bbb',
  },
});

const Map = ({ stations, edges, highlighs }) => {
  const classes = useStyles();

  const mapContainer = useRef(null);
  const [map, setMap] = useState(null);

  const drawStations = () => {
    console.log('Draw stations');
    stations.features.forEach((station) => {
      var el = document.createElement('div');
      el.className = 'marker';
      const anchorOptions = {
        element: el,
        anchor: 'bottom',
      };
      new mapboxgl.Marker(anchorOptions)
        .setLngLat(station.geometry.coordinates)
        .addTo(map);
    });
  };

  const initMap = () => {
    mapboxgl.accessToken =
      'pk.eyJ1IjoiYWxlaW5vIiwiYSI6ImNqOWk3NmtmdDM3cmQyd3FzaHZicHYzZ3MifQ.dO9dIP0fb5H62FQb0ToOSg';

    const map = new mapboxgl.Map({
      container: mapContainer.current,
      style: 'mapbox://styles/mapbox/dark-v10', // stylesheet location
      center: [24.8682152, 60.1823219],
      zoom: 12,
    });

    map.on('load', () => {
      map.addSource('stations', {
        type: 'geojson',
        data: stations,
      });

      map.addLayer({
        id: 'station-labels',
        type: 'symbol',
        source: 'stations',
        layout: {
          'text-field': ['get', 'name'],
          'text-size': 12,
          'text-font': ['Open Sans Bold', 'Arial Unicode MS Bold'],
          'text-transform': 'uppercase',
          'text-letter-spacing': 0.05,
          'text-offset': [0, 0.8],
          // 'icon-image': 'station-marker',
          // 'icon-size': 0.1,
          // 'icon-anchor': 'bottom',
          // 'icon-allow-overlap': true,
        },
        paint: {
          'text-color': '#86EA66',
        },
      });

      edges.forEach((edge) => {
        map.addSource(edge, {
          type: 'geojson',
          data: `/route-segments/${edge}.json`,
        });
      });

      edges.forEach((edge) => {
        map.addLayer({
          id: edge,
          type: 'line',
          source: edge,
          paint: {
            'line-color': '#888',
            'line-width': 2,
          },
        });
      });

      setMap(map);
    });
  };

  const highlightSegments = (highlights) => {
    console.log('higlightin');
    edges.forEach((edge) => {
      map.setPaintProperty(edge, 'line-color', '#888');
      map.setPaintProperty(edge, 'line-width', 2);
    });
    Object.entries(highlighs).forEach(([edge, color]) => {
      console.log(edge, color);
      map.setPaintProperty(edge, 'line-color', color);
      map.setPaintProperty(edge, 'line-width', 8);
    });
  };

  useEffect(() => {
    if (!map) {
      initMap();
    }
    if (map) {
      map.getSource('stations').setData(stations);
      drawStations();
    }
  }, [map, stations]);

  useEffect(() => {
    if (highlighs) {
      highlightSegments(highlighs);
    }
  }, [highlighs]);

  return <div ref={mapContainer} className={classes.map} />;
};

export default Map;
