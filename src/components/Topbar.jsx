import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    height: '64px',
    backgroundColor: '#03719C',
  },
});

const Topbar = () => {
  const classes = useStyles();
  return <nav className={classes.root}></nav>;
};

export default Topbar;
