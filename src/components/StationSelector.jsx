import React from 'react';
import { FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  stationSelector: {
    marginBottom: '48px',
  },
  formControl: {
    width: '100%',
  },
});

const StationSelector = (props) => {
  const classes = useStyles();

  const age = 10;

  return (
    <div className={classes.stationSelector}>
      <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-label">{props.label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={props.value}
          onChange={props.onChange}
          // labelWidth={100}
          // autoWidth
        >
          {props.data.map((d) => (
            <MenuItem value={d.id} key={d.id}>
              {d.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default StationSelector;
