import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { useStoreState, useStoreActions, action } from 'easy-peasy';

import StationSelector from './StationSelector';

const useStyles = makeStyles({
  routes: {
    width: '100%',
  },
  solvedRoute: {},
  totalTime: {
    display: 'flex',
    justifyContent: 'center',
    marginBottom: '48px',
    fontWeight: '600',
    fontSize: '2rem',
  },
});

const Routes = () => {
  const classes = useStyles();
  const {
    selectedDepartureStationId,
    selectedArrivalStationid,
    stations,
    solvedRoute,
  } = useStoreState((store) => store.routing);
  const { selectDepartureStation, selectArrivalStation } = useStoreActions(
    (actions) => actions.routing
  );

  const handleDepartureStationChange = (event) => {
    console.log('dep c');
    console.log(event.target.value);
    selectDepartureStation(event.target.value);
  };

  const handleArrivalStationChange = (event) => {
    console.log('arr c');
    selectArrivalStation(event.target.value);
  };
  return (
    <div className={classes.routes}>
      <StationSelector
        value={selectedDepartureStationId}
        label="Departure Station"
        data={stations}
        onChange={handleDepartureStationChange}
      />
      <StationSelector
        label="Arrival Station"
        value={selectedArrivalStationid}
        data={stations}
        onChange={handleArrivalStationChange}
      />
      {solvedRoute && (
        <div className={classes.totalTime}>
          {Math.round(solvedRoute.totalCost)} min{' '}
        </div>
      )}
      <div className={classes.solvedRoute}>
        {solvedRoute &&
          solvedRoute.stations.map((s, i) => (
            <strong key={s}>
              {s} {solvedRoute.lines[i - 1]}
              <br />
            </strong>
          ))}
      </div>
    </div>
  );
};

export default Routes;
