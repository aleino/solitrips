import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Routes from './Routes';

const useStyles = makeStyles({
  side: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    width: '400px',
    padding: '24px',
    backgroundColor: '#eee',
  },
});

const Side = () => {
  const classes = useStyles();

  return (
    <div className={classes.side}>
      <Routes />
    </div>
  );
};

export default Side;
