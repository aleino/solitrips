import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import Side from './Side';
import MapContainer from './MapContainer';

const useStyles = makeStyles({
  main: {
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    backgroundColor: 'pink',
  },
});

const Main = () => {
  const classes = useStyles();
  return (
    <div className={classes.main}>
      <Side />
      <MapContainer />
    </div>
  );
};

export default Main;
