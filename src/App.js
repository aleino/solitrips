import React from 'react';
import './App.css';

import { CssBaseline } from '@material-ui/core';

import Main from './components/Main';
import Topbar from './components/Topbar';

import { makeStyles } from '@material-ui/core/styles';

import { useStoreActions } from 'easy-peasy';

const useStyles = makeStyles({
  root: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    backgroundColor: 'orange',
  },
});

function App() {
  const classes = useStyles();
  const fetchBaseData = useStoreActions(
    (actions) => actions.routing.fetchBaseData
  );
  fetchBaseData();
  return (
    <div className={classes.root}>
      <CssBaseline />
      <Topbar />
      <Main />
    </div>
  );
}

export default App;
